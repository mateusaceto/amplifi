## Installation

```bash
yarn install
```

or

```bash
npm install
```

## Usage

```bash
yarn start
```

or

```bash
npm start
```

Run at `localhost:3000`

## Used libraries

- **State management:** Redux Toolkit + Redux Sagas
- **UI:** Styled Components + Material UI
