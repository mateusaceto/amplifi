import { PostsPage } from "pages";

const Routes = [
  {
    path: "/",
    component: PostsPage,
    exact: true,
  },
];

export default Routes;
