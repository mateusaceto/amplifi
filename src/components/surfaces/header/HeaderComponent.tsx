import DarkModeIcon from "@mui/icons-material/DarkMode";
import LightModeIcon from "@mui/icons-material/LightMode";
import { AppBar, IconButton, Tooltip, Typography } from "@mui/material";
import { useThemeProvider } from "providers";
import React from "react";
import { StyledToolbar } from "./HeaderStyles";

export interface HeaderComponentProps {
  title?: string;
}

export const HeaderComponent: React.FC<HeaderComponentProps> = ({ title }) => {
  const { mode, changeThemeMode } = useThemeProvider();

  return (
    <AppBar position="static" color="transparent">
      <StyledToolbar>
        <Typography variant="h6" component="div">
          {title ?? ""}
        </Typography>
        <Tooltip title="Change Theme Mode">
          <IconButton onClick={changeThemeMode}>
            {mode === "light" ? <DarkModeIcon /> : <LightModeIcon />}
          </IconButton>
        </Tooltip>
      </StyledToolbar>
    </AppBar>
  );
};

export default HeaderComponent;
