import { Divider, Paper, Table, TableContainer, Toolbar } from "@mui/material";
import React, { ReactNode } from "react";
import TableEmptyComponent from "./components/tableEmpty/TableEmptyComponent";
import TableHeaderComponent from "./components/tableHeader/TableHeaderComponent";
import TableLoadingComponent from "./components/tableLoading/TableLoadingComponent";
import TableRowsComponent from "./components/tableRows/TableRowsComponent";
import { UseTableComponentProvider } from "./hooks/useTableComponent";

export type TableSortType = "asc" | "desc";

export interface TableColsType {
  title: string;
  key: string | number;
  sort?: TableSortType;
}

export interface TableRowsType {
  [key: TableColsType["key"]]: string | ReactNode;
}

export interface TableConfig {
  emptyMessage?: string;
}

export interface TableComponentProps {
  rows: TableRowsType[];
  cols: TableColsType[];
  toolbar?: ReactNode;
  loading?: boolean;
  config?: TableConfig;
  onSort?: (column: string | number, type: TableSortType) => void;
}

export const TableComponent: React.FC<TableComponentProps> = (props) => {
  const { toolbar } = props;
  return (
    <UseTableComponentProvider {...props}>
      <TableContainer component={Paper}>
        {toolbar && (
          <>
            <Toolbar>{toolbar}</Toolbar>
            <Divider variant="middle" />
          </>
        )}
        <Table>
          <TableHeaderComponent />
          <TableRowsComponent />
        </Table>
        <TableLoadingComponent />
        <TableEmptyComponent />
      </TableContainer>
    </UseTableComponentProvider>
  );
};

export default TableComponent;
