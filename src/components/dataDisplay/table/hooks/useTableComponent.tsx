import React, { createContext, useContext } from "react";
import { TableComponentProps } from "../TableComponent";

export interface IUseTableComponentContextManager {}
export type IUseTableComponentContext = IUseTableComponentContextManager &
  TableComponentProps;

const UseTableComponentInitialState: IUseTableComponentContext = {
  rows: [],
  cols: [],
};

export const UseTableComponentContext =
  createContext<IUseTableComponentContext>(UseTableComponentInitialState);

export function UseTableComponentContextManager(): IUseTableComponentContextManager {
  return {};
}

export const useTableComponent = () => useContext(UseTableComponentContext);

export const UseTableComponentProvider: React.FC<TableComponentProps> = ({
  children,
  ...rest
}) => {
  const UseTableComponentContextState = UseTableComponentContextManager();

  return (
    <UseTableComponentContext.Provider
      value={{ ...UseTableComponentContextState, ...rest }}
    >
      {children}
    </UseTableComponentContext.Provider>
  );
};
