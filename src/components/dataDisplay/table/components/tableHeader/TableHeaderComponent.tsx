import ArrowUpwardIcon from "@mui/icons-material/ArrowUpward";
import { TableCell, TableHead, TableRow, Tooltip } from "@mui/material";
import React, { useCallback } from "react";
import { useTableComponent } from "../../hooks/useTableComponent";
import { SortButton } from "./TableHeaderStyles";
import ArrowDownwardIcon from "@mui/icons-material/ArrowDownward";

export const TableHeaderComponent: React.FC = () => {
  const { cols, onSort } = useTableComponent();
  const keyGenerator = useCallback(
    (index: number, key: string | number) => `th-${index}-${key}`,
    []
  );

  return (
    <TableHead>
      <TableRow>
        {cols?.map(({ title, key, sort }, index) => (
          <TableCell key={keyGenerator(index, key)}>
            {title}
            {sort && (
              <Tooltip title="Sort">
                <SortButton
                  onClick={() => onSort?.(key, sort === "asc" ? "desc" : "asc")}
                >
                  {sort === "asc" ? <ArrowUpwardIcon /> : <ArrowDownwardIcon />}
                </SortButton>
              </Tooltip>
            )}
          </TableCell>
        ))}
      </TableRow>
    </TableHead>
  );
};

export default TableHeaderComponent;
