import { IconButton } from "@mui/material";
import styled from "styled-components";

export const SortButton = styled(IconButton)`
  margin: ${({ theme }) => theme.spacing(1)};
`;
