import { CircularProgress } from "@mui/material";
import React from "react";
import { useTableComponent } from "../../hooks/useTableComponent";
import { Root } from "./TableLoadingStyles";

export const TableLoadingComponent: React.FC = () => {
  const { loading } = useTableComponent();
  if (!loading) return null;

  return (
    <Root>
      <CircularProgress />
    </Root>
  );
};

export default TableLoadingComponent;
