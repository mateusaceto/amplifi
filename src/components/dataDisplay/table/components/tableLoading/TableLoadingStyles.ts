import styled from "styled-components";

export const Root = styled.div`
  padding: ${({ theme }) => theme.spacing(4)};
  display: flex;
  align-items: center;
  justify-content: center;
`;
