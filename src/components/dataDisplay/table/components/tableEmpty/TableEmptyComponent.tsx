import { Typography } from "@mui/material";
import React from "react";
import { useTableComponent } from "../../hooks/useTableComponent";
import { Root } from "./TableEmptyStyles";
import FormatListBulletedIcon from "@mui/icons-material/FormatListBulleted";

export const TableEmptyComponent: React.FC = () => {
  const { rows, loading, config } = useTableComponent();

  if (loading || rows.length > 0) return null;

  return (
    <Root>
      <FormatListBulletedIcon />
      <Typography variant="subtitle2">
        {config?.emptyMessage ?? "No data to display."}
      </Typography>
    </Root>
  );
};

export default TableEmptyComponent;
