import styled from "styled-components";

export const Root = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  padding: ${({ theme }) => theme.spacing(4)};
  flex-direction: column;
  gap: ${({ theme }) => theme.spacing(2)};
`;
