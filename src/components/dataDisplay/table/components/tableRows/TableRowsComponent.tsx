import { TableBody, TableCell, TableRow } from "@mui/material";
import React, { useCallback } from "react";
import { useTableComponent } from "../../hooks/useTableComponent";

export const TableRowsComponent: React.FC = () => {
  const { cols, rows } = useTableComponent();
  const keyGenerator = useCallback((index: number) => `tr-${index}`, []);
  const colKeyGenerator = useCallback(
    (index: number, key: string | number) => `tc-${index}-${key}`,
    []
  );
  return (
    <TableBody>
      {rows?.map((row, index) => (
        <TableRow key={keyGenerator(index)}>
          {cols?.map(({ key }) => (
            <TableCell key={colKeyGenerator(index, key)}>{row[key]}</TableCell>
          ))}
        </TableRow>
      ))}
    </TableBody>
  );
};

export default TableRowsComponent;
