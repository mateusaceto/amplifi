import { TableComponent } from "components";
import { DefaultTemplate } from "layouts/templates";
import React from "react";
import { PostsPageProvider, usePostsPage } from "./hooks/usePostsPage";

export const PostsComponent: React.FC = () => {
  const { rows, cols, loading } = usePostsPage();

  return (
    <DefaultTemplate headerProps={{ title: "Posts" }}>
      <TableComponent {...{ rows, cols, loading }} />
    </DefaultTemplate>
  );
};

export const PostsPage: React.FC = () => {
  return (
    <PostsPageProvider>
      <PostsComponent />
    </PostsPageProvider>
  );
};

export default PostsPage;
