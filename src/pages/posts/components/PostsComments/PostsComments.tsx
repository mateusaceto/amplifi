import CommentIcon from "@mui/icons-material/Comment";
import {
  Avatar,
  Box,
  IconButton,
  List,
  ListItem,
  ListItemAvatar,
  ListItemText,
  Popover,
  Skeleton,
} from "@mui/material";
import { PostsModel } from "models";
import { usePostsPage } from "pages/posts/hooks/usePostsPage";
import React, { useCallback, useEffect, useMemo } from "react";
import { useSelector } from "react-redux";
import { memoPostsSelector } from "store/posts";
import { CommentsRoot } from "./PostsCommentsStyles";

export interface PostsCommentsProps {
  post: PostsModel;
}

export const PostsComments: React.FC<PostsCommentsProps> = ({ post }) => {
  const { getComments } = usePostsPage();
  const [anchorEl, setAnchorEl] = React.useState<HTMLButtonElement | null>(
    null
  );
  const loading = useSelector(memoPostsSelector.loadingComments);

  const open = useMemo(() => Boolean(anchorEl), [anchorEl]);

  const handleCommentsOpen = useCallback(
    (event: React.MouseEvent<HTMLButtonElement>) => {
      setAnchorEl(event.currentTarget);
    },
    [setAnchorEl]
  );

  const handleCommentsClose = useCallback(() => {
    setAnchorEl(null);
  }, [setAnchorEl]);

  useEffect(() => {
    if (open) getComments(post.id);
  }, [open, getComments, post?.id]);

  return (
    <>
      <IconButton onClick={handleCommentsOpen}>
        <CommentIcon />
      </IconButton>
      <Popover
        id={post?.id}
        open={open}
        anchorEl={anchorEl}
        onClose={handleCommentsClose}
        anchorOrigin={{
          vertical: "bottom",
          horizontal: "left",
        }}
      >
        <CommentsRoot>
          <List>
            {loading && (
              <Box>
                <Skeleton animation="wave" height={40} />
                <Skeleton animation="wave" height={40} />
                <Skeleton animation="wave" height={40} />
                <Skeleton animation="wave" height={40} />
              </Box>
            )}
            {!post?.comments ||
              (post.comments.length === 0 && !loading && (
                <ListItem>
                  <ListItemAvatar>
                    <Avatar>
                      <CommentIcon />
                    </Avatar>
                  </ListItemAvatar>
                  <ListItemText primary={"No comments yet"} />
                </ListItem>
              ))}
            {post?.comments?.map((comment) => (
              <ListItem key={comment.id}>
                <ListItemAvatar>
                  <Avatar>
                    <CommentIcon />
                  </Avatar>
                </ListItemAvatar>
                <ListItemText
                  primary={comment.email}
                  secondary={comment.body}
                />
              </ListItem>
            ))}
          </List>
        </CommentsRoot>
      </Popover>
    </>
  );
};

export default PostsComments;
