import styled from "styled-components";

export const CommentsRoot = styled.div`
  min-width: 300px;
  max-width: 600px;
  padding: ${({ theme }) => theme.spacing(2)};
`;
