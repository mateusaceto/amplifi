import { TableColsType, TableRowsType } from "components";
import { usePosts } from "hooks";
import React, { createContext, useContext, useEffect, useMemo } from "react";
import { PostsComments } from "../components";

export interface IUsePostsPageContext {
  rows: TableRowsType[];
  cols: TableColsType[];
  loading: boolean;
  getComments: ReturnType<typeof usePosts>["getComments"];
}

const UsePostsPageInitialState: IUsePostsPageContext = {
  rows: [],
  cols: [],
  loading: true,
  getComments: () => {},
};

export const UsePostsPageContext = createContext<IUsePostsPageContext>(
  UsePostsPageInitialState
);

export function UsePostsPageContextManager(): IUsePostsPageContext {
  const { posts, getData, loading, getComments } = usePosts();

  useEffect(() => {
    getData();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const cols: TableColsType[] = useMemo(
    () => [
      {
        key: "title",
        title: "Title",
      },
      {
        key: "body",
        title: "Body",
      },
      {
        key: "comments",
        title: "Comments",
      },
    ],
    []
  );

  const rows: TableRowsType[] = useMemo(
    () =>
      posts?.map((post) => ({
        title: post.title,
        body: post.body,
        comments: <PostsComments post={post} />,
      })),
    [posts]
  );

  return {
    rows,
    cols,
    loading,
    getComments,
  };
}

export const usePostsPage = () => useContext(UsePostsPageContext);

export const PostsPageProvider: React.FC = ({ children }) => {
  return (
    <UsePostsPageContext.Provider value={UsePostsPageContextManager()}>
      {children}
    </UsePostsPageContext.Provider>
  );
};
