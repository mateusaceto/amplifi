import { createTheme } from "@mui/material/styles";

export type ThemeMode = "light" | "dark";

export const theme = (mode: ThemeMode) =>
  createTheme({
    palette: {
      background: {
        default: mode === "light" ? "#fafafa" : "#212121",
        paper: mode === "light" ? "#fafafa" : "#212121",
      },
      mode: mode,
      primary: {
        light: "#81d4fa",
        main: "#0288d1",
        dark: "#01579b",
        contrastText: "#fff",
      },
      secondary: {
        light: "#cfd8dc",
        main: "#455a64",
        dark: "#263238",
        contrastText: "#fff",
      },
    },
    typography: {
      allVariants: {
        color: mode === "dark" ? "#fafafa" : "#212121",
      },
    },
  });

export type ThemeType = ReturnType<typeof theme>;

declare module "styled-components" {
  export interface DefaultTheme extends ReturnType<typeof theme> {}
}
