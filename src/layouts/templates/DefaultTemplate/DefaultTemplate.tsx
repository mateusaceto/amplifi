import { HeaderComponent, HeaderComponentProps } from "components";
import React from "react";
import { Container, Content, FinalGap } from "./DefaultTemplateStyles";

export interface DefaultTemplateProps {
  headerProps?: HeaderComponentProps;
  withoutPadding?: boolean;
}

export const DefaultTemplate: React.FC<DefaultTemplateProps> = ({
  children,
  headerProps,
  withoutPadding,
}) => {
  return (
    <Container>
      <HeaderComponent {...headerProps} />
      <Content withoutPadding={withoutPadding}>{children}</Content>
      <FinalGap />
    </Container>
  );
};

export default DefaultTemplate;
