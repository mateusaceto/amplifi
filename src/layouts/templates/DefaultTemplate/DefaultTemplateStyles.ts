import styled from "styled-components";
import { DefaultTemplateProps } from "..";

export const Container = styled.div`
  height: 100%;
  overflow: auto;
  overflow-x: hidden;
  background-color: ${({ theme }) => theme.palette.background.default};
`;
export const Content = styled.div<Partial<DefaultTemplateProps>>`
  padding: ${({ theme, withoutPadding }) =>
    withoutPadding ? 0 : theme.spacing(2)};
  padding-bottom: 0;
`;

export const FinalGap = styled.div`
  height: ${({ theme }) => theme.spacing(4)};
`;
