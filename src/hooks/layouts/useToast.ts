import { toast } from "react-toastify";

export interface IUseToast {
  showToast: (message: string, options?: Parameters<typeof toast>[1]) => void;
}

export function useToast(): IUseToast {
  const showToast = (message: string, options?: Parameters<typeof toast>[1]) =>
    toast(message, options);

  return {
    showToast,
  };
}
