import { PostsModel } from "models";
import { useCallback, useMemo } from "react";
import { useDispatch, useSelector } from "react-redux";
import { postsActions, postsSelector } from "store/posts";

export function usePosts() {
  const posts = useSelector(postsSelector.posts);
  const status = useSelector(postsSelector.getStatus);
  const dispatch = useDispatch();

  const loading = useMemo(() => status === "loading", [status]);

  const getData = useCallback(() => {
    dispatch(postsActions.getRequest());
  }, [dispatch]);

  const getComments = useCallback(
    (postId: PostsModel["id"]) => {
      dispatch(postsActions.getCommentsRequest({ id: postId }));
    },
    [dispatch]
  );

  return {
    getData,
    loading,
    posts,
    getComments,
  };
}
