import { configureStore } from "@reduxjs/toolkit";
import createSagaMiddleware from "redux-saga";
import { rootReducer } from "./rootReducer";
import { rootSaga } from "./rootSaga";
import { rootState } from "./rootState";

const sagaMiddleware = createSagaMiddleware();
/**
 * Put all redux middlewares into this array.
 */
const reduxMiddlewares = [sagaMiddleware];

/**
 * @description Redux Store creation and initial config
 */

export const RootStore = configureStore({
  reducer: rootReducer,
  preloadedState: rootState,
  devTools: process.env.NODE_ENV !== "production",
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({
      thunk: false,
      serializableCheck: false,
    }).concat(reduxMiddlewares),
});

sagaMiddleware.run(rootSaga);
