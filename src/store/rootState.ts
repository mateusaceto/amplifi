import { initialState as posts } from "./posts";

/**
 * @description App initial redux state
 */
export const rootState = {
  posts,
};
