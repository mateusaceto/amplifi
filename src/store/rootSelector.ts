import { postsSelector, memoPostsSelector } from "./posts";

export const rootSelectors = {
  posts: postsSelector,
};

export const rootMemoSelectors = {
  posts: memoPostsSelector,
};
