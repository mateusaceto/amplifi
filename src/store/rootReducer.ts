import { combineReducers } from "@reduxjs/toolkit";
import { postsSlice } from "./posts";

/**
 * @description App root reducer.
 */
export const rootReducer = combineReducers({
  posts: postsSlice.reducer,
});

export type RootState = ReturnType<typeof rootReducer>;
