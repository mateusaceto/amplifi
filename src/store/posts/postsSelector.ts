import { createSelector } from "@reduxjs/toolkit";
import { RootState } from "store";

export const postsSelector = {
  posts: (state: RootState) => state.posts.data,
  getStatus: (state: RootState) => state.posts.getStatus,
  setStatus: (state: RootState) => state.posts.setStatus,
  error: (state: RootState) => state.posts.error,
  commentsStatus: (state: RootState) => state.posts.comments.getStatus,
};

export const memoPostsSelector = {
  posts: createSelector([postsSelector.posts], (posts) => posts),
  getLoading: createSelector([postsSelector.getStatus], (status) =>
    Boolean(status === "loading")
  ),
  setLoading: createSelector([postsSelector.setStatus], (status) =>
    Boolean(status === "loading")
  ),
  getError: createSelector(
    [postsSelector.setStatus, postsSelector.error],
    (status, error) => (Boolean(status === "error") ? error : null)
  ),
  setError: createSelector(
    [postsSelector.setStatus, postsSelector.error],
    (status, error) => (Boolean(status === "error") ? error : null)
  ),
  loadingComments: createSelector([postsSelector.commentsStatus], (status) =>
    Boolean(status === "loading")
  ),
};
