import { CommentModel, PostsModel } from "models";
import { all, put, takeLatest } from "redux-saga/effects";
import { ServiceFactory } from "services";
import { postsActions, PostsActionTypes, PostsTypes } from "./postsSlice";

function* getPosts() {
  try {
    const PostsService = ServiceFactory.getPostsService();
    const data: PostsModel[] = yield PostsService.get();
    yield put(postsActions.getSuccess({ data }));
  } catch (error) {
    yield put(
      postsActions.getFailure({
        error: String(error),
      })
    );
  }
}

function* getComments(action: PostsActionTypes["getCommentsRequest"]) {
  try {
    const CommentsService = ServiceFactory.getCommentsService();
    const data: CommentModel[] = yield CommentsService.get({
      postId: action.payload.id,
    });

    yield put(
      postsActions.getCommentsSuccess({ data, postId: action.payload.id })
    );
  } catch (error) {
    yield put(
      postsActions.getFailure({
        error: String(error),
      })
    );
  }
}

export function* postsSaga() {
  yield all([
    takeLatest<PostsTypes, any>("@amplifi/posts/getRequest", getPosts),
    takeLatest<PostsTypes, any>(
      "@amplifi/posts/getCommentsRequest",
      getComments
    ),
  ]);
}
