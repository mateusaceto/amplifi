import { PayloadAction } from "@reduxjs/toolkit";
import { CommentModel, PostsModel } from "models";
import {
  createAbstractSlice,
  GenericState,
  GenericStatus,
} from "store/rootSlice";
import { GenericActionsTypes, GenericTypes } from "store/types";

const name = "@amplifi/posts";

interface CommentsState {
  comments: {
    getStatus: GenericStatus;
  };
}

type PostsState = GenericState<PostsModel, CommentsState>;

export const initialState: PostsState = {
  data: [],
  getStatus: "loading",
  setStatus: null,
  error: null,
  comments: {
    getStatus: "loading",
  },
};

interface GetCommentsRequestPayload {
  id: PostsModel["id"];
}

interface GetCommentsSuccessPayload {
  data: CommentModel[];
  postId: PostsModel["id"];
}

export const postsSlice = createAbstractSlice({
  name,
  initialState,
  reducers: {
    getCommentsRequest(
      state: PostsState,
      _: PayloadAction<GetCommentsRequestPayload>
    ) {
      state.comments.getStatus = "loading";
    },
    getCommentsSuccess(
      state: PostsState,
      payload: PayloadAction<GetCommentsSuccessPayload>
    ) {
      state.comments.getStatus = "success";
      const postIndex = state.data
        .map((e) => e.id)
        .indexOf(payload.payload.postId);
      if (postIndex !== -1) {
        state.data[postIndex].setComments(payload.payload.data);
      }
      return state;
    },
  },
});

export const postsActions = postsSlice.actions;
export type PostsTypes = GenericTypes<typeof postsActions, typeof name>;
export type PostsActionTypes = GenericActionsTypes<
  typeof postsActions,
  typeof name
>;
