import { CaseReducerActions, PayloadAction } from "@reduxjs/toolkit";

/**
 * @type  Redux Slice Types
 */
export type GenericTypes<
  T extends CaseReducerActions<{ [key: string]: (...args: any) => any }>,
  N extends string
> = keyof {
  [Property in keyof T as `${N}/${Property & string}`]: any;
};

/**
 * @type  Redux Slice Actions Types
 */
export type GenericActionsTypes<
  T extends CaseReducerActions<{ [key: string]: (...args: any) => any }>,
  N extends string
> = {
  [Property in keyof T]: PayloadAction<
    Parameters<T[Property]>[0],
    `${N}/${Property & string}`
  >;
};
