import { all, call, spawn } from "redux-saga/effects";
import { postsSaga } from "./posts";

export function* rootSaga() {
  const sagas: any[] = [postsSaga];
  yield all(
    sagas.map((saga) =>
      spawn(function* () {
        while (true) {
          try {
            yield call(saga);
            break;
          } catch (e) {
            //
          }
        }
      })
    )
  );
}
