import {
  createSlice,
  PayloadAction,
  SliceCaseReducers,
  ValidateSliceCaseReducers,
} from "@reduxjs/toolkit";

export type GenericStatus = "loading" | "success" | "error";
export type GenericMethods = "PUT" | "STORE" | "DELETE";

export type GenericState<T, A> = A & {
  data: T[];
  getStatus: GenericStatus | null;
  setStatus: GenericStatus | null;
  error: string | null;
};

export interface GetSuccessPayload<T> {
  data: T[];
}

export interface GetFailurePayload {
  error: string | null;
}

export interface SetRequestPayload<T> {
  data?: T;
  id?: string;
  method: GenericMethods;
}

export interface SetSuccessPayload<T> extends SetRequestPayload<T> {}

export interface SetFailurePayload extends GetFailurePayload {}

/**
 * @description Abstract Redux Slice, to make it easier to create Redux slices.
 * @description Creates common custom data template requests actions.
 */
export const createAbstractSlice = <
  T extends { id?: string },
  A extends { [key: string]: any },
  Reducers extends SliceCaseReducers<GenericState<T, A>>
>({
  name = "",
  initialState,
  reducers,
}: {
  name: string;
  initialState: GenericState<T, A>;
  reducers: ValidateSliceCaseReducers<GenericState<T, A>, Reducers>;
}) => {
  return createSlice({
    name,
    initialState,
    reducers: {
      getRequest: (state) => {
        state.getStatus = "loading";
      },
      getSuccess: (
        state: GenericState<T, A>,
        action: PayloadAction<GetSuccessPayload<T>>
      ) => {
        state.data = action.payload.data;
        state.getStatus = "success";
      },
      getFailure: (
        state: GenericState<T, A>,
        action: PayloadAction<GetFailurePayload>
      ) => {
        state.error = action.payload.error;
        state.getStatus = "error";
      },
      setRequest: (state, action: PayloadAction<SetRequestPayload<T>>) => {
        state.setStatus = "loading";
      },
      setSuccess: (
        state: GenericState<T, A>,
        action: PayloadAction<SetSuccessPayload<T>>
      ) => {
        switch (action.payload.method) {
          case "STORE":
            const currentData = state.data ?? [];
            if (action.payload.data)
              state.data = [...currentData, action.payload.data];
            break;
          case "PUT":
            state.data = state.data.map((item) => {
              if (item.id === action.payload.data?.id) {
                return action.payload.data ?? item;
              }
              return item;
            });
            break;
          case "DELETE":
            state.data = state.data.filter(
              (item) => item.id !== action.payload.id
            );
            break;
          default:
            break;
        }
        state.setStatus = "success";
      },
      setFailure: (
        state: GenericState<T, A>,
        action: PayloadAction<SetFailurePayload>
      ) => {
        state.error = action.payload.error;
        state.setStatus = "error";
      },
      ...reducers,
    },
  });
};
