import { getUUID } from "utils";

export abstract class EntryModel {
  id?: string;

  constructor(data?: Partial<EntryModel>) {
    this.id = data?.id ?? getUUID().substring(0, 6);
  }
}
