import { EntryModel } from "./EntryModel";
import { PostsModel } from "./PostsModel";

export class CommentModel extends EntryModel {
  postId: PostsModel["id"] | null = null;
  name: string | null = null;
  email: string | null = null;
  body: string | null = null;

  constructor(args: Partial<CommentModel>) {
    super(args);
    Object.assign(this, args);
  }
}
