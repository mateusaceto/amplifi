import { CommentModel } from "./CommentModel";
import { EntryModel } from "./EntryModel";

export class PostsModel extends EntryModel {
  userId: number | null = null;
  title: string | null = null;
  body: string | null = null;
  comments?: CommentModel[] = [];

  constructor(args: Partial<PostsModel>) {
    super(args);
    Object.assign(this, args);
  }

  setComments(comments: CommentModel[]): void {
    this.comments = comments;
  }
}
