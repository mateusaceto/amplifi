import { PostsModel } from "models/PostsModel";
import { HttpService } from "services";

export class PostsService extends HttpService<PostsModel> {
  route = "/posts";

  override formatData(data: Partial<PostsModel>[]): PostsModel[] {
    return data?.map((item) => new PostsModel(item));
  }

  static instance: PostsService = new PostsService();
}
