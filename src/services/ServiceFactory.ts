/**
 * @description Central Services Factory
 */

import { CommentsService } from "./comments/CommentsService";
import { PostsService } from "./posts/PostsService";

export class ServiceFactory {
  static getPostsService = () => PostsService.instance;
  static getCommentsService = () => CommentsService.instance;
}
