/**
 * @description Service to handle http requests
 */
export class HttpService<T> {
  url: string = "https://jsonplaceholder.typicode.com";
  route: string = "";

  get fullUrl() {
    return this.url + this.route;
  }

  protected formatData(data: T[]) {
    return data;
  }

  private async fetchRequest({
    method,
    config,
    formatedUrl,
  }: {
    method?: "GET" | "POST" | "PUT" | "DELETE";
    config?: RequestInit;
    formatedUrl?: string;
  } = {}): Promise<T | T[]> {
    try {
      const res = await fetch(formatedUrl ?? this.fullUrl, {
        method: method ?? "GET",
        ...(config ?? {}),
      });
      if (!res.ok) {
        const errorMessage = await res.text();
        return Promise.reject(errorMessage);
      }
      const data = await res.json();

      return Promise.resolve(this.formatData(data));
    } catch (e) {
      return Promise.reject(e);
    }
  }

  async get(params?: { [key: string]: any }) {
    try {
      const formatedParams = params && new URLSearchParams(params).toString();
      const formatedUrl = `${this.fullUrl}${
        formatedParams ? `?${formatedParams}` : ""
      }`;
      const response = await this.fetchRequest({ formatedUrl });
      return response;
    } catch (e) {
      return Promise.reject(e);
    }
  }

  async post({ data }: { data: T }) {
    try {
      const response = await this.fetchRequest({
        method: "POST",
        config: { body: JSON.stringify(data) },
      });
      return response;
    } catch (e) {
      return Promise.reject(e);
    }
  }

  async put({ data, id }: { data: T; id: string }) {
    try {
      const formatedUrl = `${this.fullUrl}/${id}`;
      const response = await this.fetchRequest({
        method: "PUT",
        formatedUrl,
        config: { body: JSON.stringify(data) },
      });
      return response;
    } catch (e) {
      return Promise.reject(e);
    }
  }

  async delete({ id }: { id: string }) {
    try {
      const formatedUrl = `${this.fullUrl}/${id}`;
      const response = await this.fetchRequest({
        method: "DELETE",
        formatedUrl,
      });
      return response;
    } catch (e) {
      return Promise.reject(e);
    }
  }
}
