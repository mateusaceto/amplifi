import { CommentModel } from "models";
import { HttpService } from "services";

export class CommentsService extends HttpService<CommentModel> {
  route = "/comments";

  override formatData(data: Partial<CommentModel>[]): CommentModel[] {
    return data?.map((item) => new CommentModel(item));
  }

  static instance: CommentsService = new CommentsService();
}
