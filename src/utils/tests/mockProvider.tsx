import { ThemeProvider } from "providers";
import React from "react";

export interface MockProviderProps {
  children?: React.ReactNode | React.ReactNode[];
}

export const MockProvider: React.FC<MockProviderProps> = ({ children }) => {
  return <ThemeProvider>{children}</ThemeProvider>;
};

export default MockProvider;
