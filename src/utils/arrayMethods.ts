export function sortArray<T>({
  array,
  direction,
  key,
}: {
  array: T & Array<any>;
  direction?: "asc" | "desc";
  key?: string | number;
}): T {
  if (!Array.isArray(array) || !array || array.length <= 0) return array;

  return array.sort((a, b) => {
    if (key ? a[key] < b[key] : a < b) {
      return direction === "desc" ? 1 : -1;
    }
    if (key ? a[key] > b[key] : a > b) {
      return direction === "desc" ? -1 : 1;
    }
    return 0;
  });
}
