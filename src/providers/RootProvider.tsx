import React, { ReactNode } from "react";
import { Provider } from "react-redux";
import { BrowserRouter } from "react-router-dom";
import { RootStore } from "store";
import { ThemeProvider } from "./ThemeProvider";
import ToastProvider from "./ToastProvider";
import { ConfirmProvider } from "material-ui-confirm";

export interface RootProviderProps {
  children: ReactNode | ReactNode[];
}

/**
 * App React Providers wrapper
 */
export const RootProvider: React.FC<RootProviderProps> = ({ children }) => {
  return (
    <Provider store={RootStore}>
      <BrowserRouter>
        <ThemeProvider>
          <ConfirmProvider>{children}</ConfirmProvider>
          <ToastProvider />
        </ThemeProvider>
      </BrowserRouter>
    </Provider>
  );
};

export default RootProvider;
