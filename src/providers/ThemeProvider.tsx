import { theme, ThemeMode } from "layouts/theme";
import React, {
  createContext,
  useCallback,
  useContext,
  useMemo,
  useState,
} from "react";
import { ThemeProvider as StyleComponentsThemeProvider } from "styled-components";
import { ThemeProvider as MUIThemeProvider } from "@mui/material/styles";

export interface IThemeProviderContext {
  mode: ThemeMode;
  changeThemeMode: () => void;
}

const ThemeProviderInitialState: IThemeProviderContext = {
  mode: "light",
  changeThemeMode: () => {},
};

export const ThemeProviderContext = createContext<IThemeProviderContext>(
  ThemeProviderInitialState
);

export function ThemeProviderContextManager(): IThemeProviderContext {
  const [mode, setMode] = useState<ThemeMode>(ThemeProviderInitialState.mode);

  const changeThemeMode = useCallback(
    () => setMode((prevState) => (prevState === "light" ? "dark" : "light")),
    [setMode]
  );

  return {
    mode,
    changeThemeMode,
  };
}

export const useThemeProvider = () => useContext(ThemeProviderContext);

export const ThemeProvider: React.FC = ({ children }) => {
  const ThemeState = ThemeProviderContextManager();
  const currentTheme = useMemo(() => theme(ThemeState.mode), [ThemeState.mode]);

  return (
    <ThemeProviderContext.Provider value={ThemeState}>
      <MUIThemeProvider theme={currentTheme}>
        <StyleComponentsThemeProvider theme={currentTheme}>
          {children}
        </StyleComponentsThemeProvider>
      </MUIThemeProvider>
    </ThemeProviderContext.Provider>
  );
};
