import React from "react";
import { ToastContainer } from "react-toastify";

export interface ToastProviderProps {}

export const ToastProvider: React.FC<ToastProviderProps> = () => {
  return (
      <ToastContainer
        position="top-left"
        autoClose={3000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
      />
  );
};

export default ToastProvider;
